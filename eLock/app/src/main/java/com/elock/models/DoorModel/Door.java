package com.elock.models.DoorModel;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.json.JSONException;
import org.json.JSONObject;

@Entity(tableName = "door")
public class Door {

    @PrimaryKey()
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "doorID")
    private String doorID;

    @ColumnInfo(name = "key")
    private String key;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "state")
    private String state;


    public Door(int id, String doorID, String key, String name, String state) {
        this.id = id;
        this.doorID = doorID;
        this.key = key;
        this.name = name;
        this.state = state;
    }

    @Ignore
    public Door(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getInt("id");
        this.doorID = jsonObject.getString("idporta");
        this.key = jsonObject.getString("chave");
        this.name = jsonObject.isNull("nome") ? "": jsonObject.getString("nome");
        this.state = jsonObject.getString("estado");
    }


    public int getId() {
        return id;
    }

    public String getDoorID() {
        return doorID;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    @Ignore
    public void setName(String name){
        this.name = name;
    }

    @Ignore
    @Override
    public String toString() {
        return "Door{" +
                "id=" + id +
                ", doorID='" + doorID + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
