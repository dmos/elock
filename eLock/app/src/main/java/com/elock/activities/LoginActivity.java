package com.elock.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.elock.R;
import com.elock.TopBarClass;
import com.elock.models.UserModel.User;

public class LoginActivity extends AppCompatActivity {

    private TextView editTextEmail;
    private TextView editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        new TopBarClass(this,findViewById(R.id.topbarView),"Login",false,false);


        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);

    }

    public void tryLogin(View v){

        User user = new User(this);

        if(user.tryLogin(editTextEmail.getText().toString(),this.editTextPassword.getText().toString())){
            gotoDashboard();
        }
        else {
            editTextPassword.setText("");
        }

    }

    private void gotoDashboard(){
        Intent dashboardIntent = new Intent(this,DashboardActivity.class);
        startActivity(dashboardIntent);
    }

}
