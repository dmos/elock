package com.elock.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.elock.R;
import com.elock.TopBarClass;
import com.elock.models.DoorModel.Door;
import com.elock.models.DoorModel.DoorDatabase;

public class EditDoorActivity extends AppCompatActivity {
    private static final String TAG = "EditDoorActivity";

    TextView editTextName;
    Door door;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_door);

        //create topbar
        new TopBarClass(this,findViewById(R.id.topbarView),"Editar Door",true,false);

        int id = getIntent().getExtras().getInt("id");

         door = DoorDatabase.getDoorID(id);

        editTextName = findViewById(R.id.editTextName);
        editTextName.setText(door.getName());

    }

    public void save(View v){
        door.setName(editTextName.getText().toString());

        if(DoorDatabase.updateDoorServer(door)){
            finish();
        }

    }
}
