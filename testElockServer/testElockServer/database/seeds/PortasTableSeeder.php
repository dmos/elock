<?php

use Illuminate\Database\Seeder;

class PortasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portas')->insert([
            'idporta'=>"idPorta1",
            'chave'=>"chave1",
            'nome'=>"c1",
            'estado'=>"Com Acesso"
	    ]);

	    DB::table('portas')->insert([
            'idporta'=>"idPorta2",
            'chave'=>"chave2",
            'nome'=>"c2",
            'estado'=>"Sem Acesso"
        ]);
        
        DB::table('portas')->insert([
            'idporta'=>"idPorta1",
            'chave'=>"chave3",
            'nome'=>"c3",
            'estado'=>"Pedido"
	    ]);
    }
}
