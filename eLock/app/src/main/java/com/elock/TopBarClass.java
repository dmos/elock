package com.elock;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.elock.activities.SettingsActivity;

public class TopBarClass {
    ImageView back;
    ImageView settings;
    TextView actName;
    Context context;

    public TopBarClass(final Context context, View topbarView, String actName, boolean showBack, boolean showSettings) {
        this.context = context;
        this.back = topbarView.findViewById(R.id.back);
        this.settings = topbarView.findViewById(R.id.settings);
        this.actName = topbarView.findViewById(R.id.actName);
        this.actName.setText(actName);

        if (showBack){
            this.back.setVisibility(View.VISIBLE);
            this.back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((Activity) context).finish();
                }
            });
        }

        if(showSettings){
            this.settings.setVisibility(View.VISIBLE);
            this.settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent settingsIntent = new Intent(context, SettingsActivity.class);
                    context.startActivity(settingsIntent);
                }
            });
        }

    }


}
