package com.elock.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.elock.R;
import com.elock.TopBarClass;
import com.elock.models.DoorModel.Door;
import com.elock.models.DoorModel.DoorDatabase;

public class OpenDoorActivity extends AppCompatActivity {
    private static final String TAG = "OpenDoorActivity";

    TextView lblIdNameDoor;

    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_door);

        //create topbar
        new TopBarClass(this,findViewById(R.id.topbarView),"Open Door",true,false);

        int id = getIntent().getExtras().getInt("id");

        Door door = DoorDatabase.getDoorID(id);

        lblIdNameDoor = findViewById(R.id.lblIdNameDoor);
        lblIdNameDoor.setText(door.getDoorID() + " - " + door.getName());

        // definir chave em shared prefrences para usar no cardService
        sp = this.getSharedPreferences("door",MODE_PRIVATE);
        sp.edit().putString("key",door.getKey()).apply();


    }

    @Override
    protected void onStop() {
        super.onStop();
        sp.edit().clear().apply();
        Log.e(TAG, "onStop");
    }

    public void cancelDoor(View v){
        finish();
    }


}