package com.elock.modules;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.elock.R;
import com.elock.models.UserModel.User;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class apiCalls extends AsyncTask<String,String,String> {

    private static final String TAG = "APICALL";
    private ProgressDialog pd;
    private String progressDialogMessage;
    private Context context;
    private String apiEndpoint;
    private String method;
    private User user;

    public apiCalls(Context context, String progressDialogMessage, String apiEndPoint, String method) {
        this.context = context;
        this.progressDialogMessage = progressDialogMessage;
        this.apiEndpoint = context.getString(R.string.Connection).toString() + apiEndPoint;
        this.method = method;

        this.user = new User(context);
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd = new ProgressDialog(context);
        pd.setMessage(progressDialogMessage);
        pd.setCancelable(true);
        pd.show();
    }


    @Override
    protected String doInBackground(String... params) {

        if(user.isLoggin()){
            params[0] = "accesstoken="+ user.getAccessToken()+"&" + params[0];
        }

        BufferedReader reader = null;

        try {

            //URL url = new URL(strings[0]);
            URL url = new URL(apiEndpoint);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            System.out.println("connecting to: " + urlConnection.toString());
            urlConnection.setRequestMethod(method);

            if(params[0] != null){
                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out,"UTF-8"));
                writer.write(params[0]);
                writer.flush();
                writer.close();
                out.close();
                Log.e(TAG, "PARAMS :" + params[0]);
            }

            reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer strBuffer = new StringBuffer();
            String line;
            while((line=reader.readLine()) != null){
                strBuffer.append(line);
            }

            Log.i(TAG, "RESPONSE : "+ strBuffer.toString());

            return strBuffer.toString();


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onPostExecute(String s) {
        if (pd.isShowing()){
            pd.hide();
        }
    }
}
