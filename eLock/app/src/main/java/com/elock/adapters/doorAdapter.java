package com.elock.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.elock.R;
import com.elock.models.DoorModel.Door;

import java.util.List;

public class doorAdapter extends RecyclerView.Adapter<doorAdapter.DoorsViewHolder>{

    private static final String TAG = "doorAdapter";
    private List<Door> doorsList;
    private Context context;

    public doorAdapter.onDoorListner onDoorListner;

    public doorAdapter(List<Door> doorsList, Context context, doorAdapter.onDoorListner onDoorListner) {
        this.doorsList = doorsList;
        this.context = context;
        this.onDoorListner = onDoorListner;
        //System.out.println(TAG + " Constructor: called");
    }

    @NonNull
    @Override
    public DoorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View portasView = inflater.inflate(R.layout.view_doors,parent,false);


        DoorsViewHolder viewHolder = new DoorsViewHolder(portasView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DoorsViewHolder holder, int position) {
        //System.out.println(TAG + " onBindViewHolder: called");
        Door door = doorsList.get(position);
        //set text
        holder.txtIDDoor.setText(context.getString(R.string.lblIDPorta) +" "+ door.getDoorID());
        holder.txtKey.setText(context.getString(R.string.lblChave) +" "+ door.getKey());
        holder.txtDoorName.setText(context.getString(R.string.lblNomePorta) +" "+ door.getName());
        holder.txtState.setText(context.getString(R.string.lblEstado) +" "+ door.getState());

        //Log.e(TAG, "Estado -> " +estado);

        if(door.getState().equals("Com Acesso")){
            holder.stateCircle.setColorFilter(context.getResources().getColor(R.color.colorEstadoAtivo));
        }
        else if(door.getState().equals("Pedido")){
            holder.stateCircle.setColorFilter(context.getResources().getColor(R.color.colorEstadoPedido));
        }
        else{
            holder.stateCircle.setColorFilter(context.getResources().getColor(R.color.colorEstadoDesativado));
        }


        //onclick das imagens
        holder.openImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDoorListner.onOpenClick(position);
            }
        });
        holder.editImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDoorListner.onEditClick(position);
            }
        });

        holder.removeImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onDoorListner.onRemoveClick(position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return doorsList.size();
    }

    public void setDoorsList(List<Door> doorsList) {
        this.doorsList = doorsList;
        notifyDataSetChanged();
    }


    public class DoorsViewHolder extends RecyclerView.ViewHolder{
        public TextView txtIDDoor;
        public TextView txtKey;
        public TextView txtDoorName;
        public TextView txtState;

        public ImageView openImage;
        public ImageView editImage;
        public ImageView removeImage;
        public ImageView stateCircle;

        public DoorsViewHolder(@NonNull View itemView) {
            super(itemView);

            txtIDDoor = itemView.findViewById(R.id.txtIDDoor);
            txtKey = itemView.findViewById(R.id.txtKey);
            txtDoorName = itemView.findViewById(R.id.txtDoorName);
            txtState = itemView.findViewById(R.id.txtState);

            openImage = itemView.findViewById(R.id.openImage);
            editImage = itemView.findViewById(R.id.editImage);
            removeImage = itemView.findViewById(R.id.removeImage);
            stateCircle = itemView.findViewById(R.id.stateCircle);

        }

    }

    public interface onDoorListner {
        void onOpenClick(int position);
        void onEditClick(int position);
        void onRemoveClick(int position);
    }

}

