<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
		'name'=>"diogo",
		'email'=>"dmos999@gmail.com",
		'password'=>"1234",
		'accesstoken'=>"EJQ9XgDRzUlyCH8da9GxOfVKWRimSO5M"
	]);

	DB::table('users')->insert([
                'name'=>"test",
                'email'=>"test@test",
                'password'=>"test",
		'accesstoken'=>"mxbOFJFmJ605C9XLDs95Hi3aDXGFPx7t"

        ]);
    }
}
