package com.elock.models.DoorModel;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DoorDao {

    @Query("Select * from Door")
    List<Door> getAllDoors();

    @Insert
    void insertDoor(Door door);

    @Update
    void updateDoor(Door door);

    @Delete
    void deleteDoor(Door Door);

    @Query("Select * from Door where id= :id Limit 1")
    Door getDoorID(int id);

    @Query("Select * from Door where id not in(:ids)")
    List<Door> getDoorsToRemove(List<Integer> ids);


    @Query("SELECT * FROM Door")
    LiveData<List<Door>> getAllDoorsLive();

    @Query("Select * from Door where id= :id Limit 1")
    LiveData<Door> getDoorIDLive(int id);
}
