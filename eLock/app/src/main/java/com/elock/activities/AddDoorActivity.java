package com.elock.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.elock.R;
import com.elock.TopBarClass;
import com.elock.models.DoorModel.DoorDatabase;

public class AddDoorActivity extends AppCompatActivity {
    private static final String TAG = "AddDoorActivity";

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int KEY_SIZE = 14;

    EditText editTextIDDoor;
    EditText editTextKey;
    EditText editTextName;

    DoorDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_door);

        new TopBarClass(this,findViewById(R.id.topbarView),"Nova Door",true,false);

        editTextIDDoor = (EditText) findViewById(R.id.editTextIDDoor);
        editTextKey = (EditText) findViewById(R.id.editTextKey);
        editTextName = (EditText) findViewById(R.id.editTextName);

        db = DoorDatabase.getInstance(this);

    }

    public void generateKey (View v){
        int keySize = KEY_SIZE;
        StringBuilder builder = new StringBuilder();
        while (keySize-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        editTextKey.setText(builder.toString());
    }

    public void requestDoor(View v){
        //TODO : verificar inputs corretamente

        if(editTextIDDoor.getText().length() == 0 || editTextIDDoor.getText().length() == 0){
            Toast.makeText(getApplicationContext(), "Preencha o \"ID Door\" e a \"Chave\" ", Toast.LENGTH_LONG).show();
            return;
        }

        if(db.askDoorServer(editTextIDDoor.getText().toString(), editTextKey.getText().toString(),
                editTextName.getText().toString())){
            //((Activity) context).finish();
            finish();
        }
        else{
            Toast.makeText(getApplicationContext(), "Não foi possivel criar o pedido", Toast.LENGTH_SHORT).show();
        }
    }
}
