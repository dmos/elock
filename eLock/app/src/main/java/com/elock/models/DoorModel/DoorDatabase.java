package com.elock.models.DoorModel;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.elock.modules.apiCalls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Database(entities = Door.class,exportSchema = false, version = 1)
public abstract class DoorDatabase extends RoomDatabase {
    private static final String DB_NAME = "door_db";
    private static DoorDatabase instance;

    private static final String TAG = "DoorsDatabase";
    private static final String url = "/porta";
    private static Context context;

    public static synchronized DoorDatabase getInstance(Context context){
        if (instance == null){
            instance= Room.databaseBuilder(context.getApplicationContext(),
                    DoorDatabase.class,DB_NAME)
                    .build();
        }

        return instance;
    }


    public abstract DoorDao doorDao();


    public void setContext(Context context){
        this.context = context;
    }


    /* operações de base de dados simple */
    public static List<Door> getAllDoors(){
        List<Door> tmp = null;

        try{
            tmp = new AsyncTask<Void,Void,List<Door>>(){
                @Override
                protected List<Door> doInBackground(Void... voids) {
                    return instance.doorDao().getAllDoors();
                }
            }.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return tmp;
    }

    public static Door getDoorID(int id){
        Door door = null;

        try{
            door = new AsyncTask<Void,Void, Door>(){
                @Override
                protected Door doInBackground(Void... voids) {
                    return instance.doorDao().getDoorID(id);
                }
            }.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return door;
    }

    public static void insertDoor(Door door){

        try{
            new AsyncTask<Object, Object, Object>(){

                @Override
                protected Object doInBackground(Object[] objects) {
                    instance.doorDao().insertDoor(door);
                    return null;
                }
            }.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void updateDoor(Door door){
        try{
            new AsyncTask<Object, Object, Object>(){

                @Override
                protected Object doInBackground(Object[] objects) {
                    instance.doorDao().updateDoor(door);
                    return null;
                }
            }.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void deleteDoor(Door door){

        try{
            new AsyncTask<Object, Object, Object>(){

                @Override
                protected Object doInBackground(Object[] objects) {
                    instance.doorDao().deleteDoor(door);
                    return null;
                }
            }.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }


    /* atualizar base de dados e resolver conflitos */
    public static void updateDatabase(){


        try {
            apiCalls DoorsApi = (apiCalls) new apiCalls(context,"Atualizar Portas ...", url, "POST").execute("");
            String ServerResponse = DoorsApi.get();

            if (ServerResponse == null || ServerResponse.length() == 0) {
                Log.d(TAG, "updateDatabase: sem respota do servidor");
            } else {
                List<Integer> idsNotRemove = new ArrayList<>();
                JSONArray jsonArray = new JSONArray(ServerResponse);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    int id = jsonObject.getInt("id");
                    Door check = getDoorID(id);

                    Door door = new Door(jsonObject);
                    if (check == null) {
                        insertDoor(door);
                    } else {
                        updateDoor(door);
                    }
                    idsNotRemove.add(door.getId());
                }

                //remover portas não atuailizadas
                removerDoorsNotUpdated(idsNotRemove);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void removerDoorsNotUpdated(List<Integer> idsNotRemove){
        List<Door> tmp = null;

        try{
            tmp = new AsyncTask<Void,Void,List<Door>>(){
                @Override
                protected List<Door> doInBackground(Void... voids) {
                    return instance.doorDao().getDoorsToRemove(idsNotRemove);
                }
            }.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(tmp != null || tmp.size() == 0){
            for (Door door : tmp){
                deleteDoor(door);
            }
        }
    }


    /* ações de utilizador */
    public static boolean askDoorServer(String doorID, String key, String name){
        String params = "idporta="+doorID+"&chave="+key;
        if(name.length() != 0)
            params+= "&nome="+name;

        apiCalls askDoor = (apiCalls) new apiCalls(context,"a submeter","/requestDoor","POST")
                .execute(params);

        try {
            String ServerResponse = askDoor.get();
            System.out.println(TAG + "askDoorServer - APICALL SERVER RESPONSE : " + ServerResponse);

            if(ServerResponse ==null || ServerResponse.length() == 0){
                Toast.makeText(context, "sem resposta do servidor", Toast.LENGTH_SHORT).show();
            }
            else if(new JSONObject(ServerResponse).length() == 0){
                Toast.makeText(context, "Não foi possivel processar o pedido", Toast.LENGTH_SHORT).show();
            }
            else {
                JSONObject jsonObject = new JSONObject(ServerResponse);
                Door door =  new Door(jsonObject);
                insertDoor(door);
                return true;
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }



        return false;
    }

    public static boolean sendRemovedDoorServer(Door door){

        String params = "id="+door.getId()+"&idporta="+door.getDoorID();

        apiCalls sendRemovedDoor = (apiCalls) new apiCalls(context,"a submeter","/requestRemoveDoor","POST")
                .execute(params);

        try {
            String ServerResponse = sendRemovedDoor.get();
            System.out.println(TAG + "sendRemovedDoorServer - APICALL SERVER RESPONSE : " + ServerResponse);

            if(ServerResponse == null || ServerResponse.length() == 0){
                Toast.makeText(context, "sem resposta do servidor", Toast.LENGTH_SHORT).show();
            }
            else if(new JSONObject(ServerResponse).length() == 0){
                Toast.makeText(context, "Não foi possivel processar o pedido", Toast.LENGTH_SHORT).show();
            }
            else {
                deleteDoor(door);
                return true;
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean updateDoorServer(Door door){
        String params = "id="+door.getId()+"&nome="+door.getName();

        apiCalls updateDoor = (apiCalls) new apiCalls(context,"a submeter","/requestUpdateDoor","POST")
                .execute(params);

        try {
            String ServerResponse = updateDoor.get();
            System.out.println(TAG + "updatePortaServer - APICALL SERVER RESPONSE : " + ServerResponse);


            if(ServerResponse == null || ServerResponse.length() == 0){
                Toast.makeText(context, "sem resposta do servidor", Toast.LENGTH_SHORT).show();
            }
            else if(new JSONObject(ServerResponse).length() == 0){
                Toast.makeText(context, "Não foi possivel processar o pedido", Toast.LENGTH_SHORT).show();
            }
            else {
                updateDoor(door);
                return true;
            }


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }


    /* funções que utilizam LiveData */
    public LiveData<List<Door>> getAllDoorsLive(){
        return instance.doorDao().getAllDoorsLive();
    }

    public LiveData<Door> getDoorIDLive(int id){
        return instance.doorDao().getDoorIDLive(id);
    }


}
