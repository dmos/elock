<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use \App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| Test Routes
|--------------------------------------------------------------------------
*/

Route::get('/user', function (Request $request) {
	$email = $request->input("email");
	$password = $request->input("password");
	$user = App\User::where('email',$email)
			->where('password',$password)
			->get();
  	return $user;
});


Route::get('/porta', function (Request $request) {
	if(checkAccessToken($request->input("accesstoken"))){
        $porta = App\Porta::where("idporta","!=", "")
        ->get()
        ->toJson();
        return $porta;
    }
    else{
        return response()->json([]);
    }
    
});

Route::get('/requestDoor', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $idporta = $request->input("idporta");
            $chave = $request->input("chave");
            $nome = $request->input("nome");
    
            $porta = new App\Porta;
            $porta->idporta = $idporta;
            $porta->chave = $chave;
            if(!empty($nome))
                $porta->nome = $nome;
    
            $porta->estado = "Pedido";
    
            return $porta->save()? 
                $porta->toJson() :  
                response()->json([]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([]);
        }
    }
    else{
        return response()->json([]);
    }

});


Route::get('/requestRemoveDoor', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $id = $request->input("id");
            $idporta = $request->input("idporta");

            $porta = App\Porta::where("id","=",$id)
                ->where("idporta","=",$idporta);

            $porta->delete();
        
            return response()->json(["response" => "ok"]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([]);
        }
    }
    else{
        return response()->json([]); 
    }
});

Route::get('/requestUpdateDoor', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $id = $request->input("id");
            $nome = $request->input("nome");

            $porta = App\Porta::find($id);

            if(empty($nome))
                $nome="";
            
            $porta->nome = $nome;
            $porta->save();
        
            return response()->json(["response" => "ok"]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([]);
        }
    }
    else{
        return response()->json([]);
    }
});



/*
|--------------------------------------------------------------------------
| Usable Routes
|--------------------------------------------------------------------------
*/



Route::post('/user', function (Request $request) {
        $email = $request->input("email");
        $password = $request->input("password");
        $user = App\User::where('email',$email)
                        ->where('password',$password)
                        ->get();
        return $user;
});


Route::post('/porta', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        $porta = App\Porta::where("idporta","!=", "")
        ->get()
        ->toJson();
        return $porta;
    }
    else{
        return response()->json([]);
    }
});



Route::post('/requestDoor', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $idporta = $request->input("idporta");
            $chave = $request->input("chave");
            $nome = $request->input("nome");
    
            $porta = new App\Porta;
            $porta->idporta = $idporta;
            $porta->chave = $chave;
            if(!empty($nome))
                $porta->nome = $nome;
            else
                $porta->nome = "";
    
            $porta->estado = "Pedido";
    
            return $porta->save()? 
                $porta->toJson() :  
                response()->json([]);
        } catch (\Throwable $th) {
            throw $th;
            //return response()->json([]);
        }
    }
    else{
        return response()->json([]);
    }
});



Route::post('/requestRemoveDoor', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $id = $request->input("id");
            $idporta = $request->input("idporta");

            $porta = App\Porta::where("id","=",$id)
                ->where("idporta","=",$idporta);

            $porta->delete();
        
            return response()->json(["response" => "ok"]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([]);
        }
    }
    else{
        return response()->json([]); 
    }
});




Route::post('/requestUpdateDoor', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $id = $request->input("id");
            $nome = $request->input("nome");

            $porta = App\Porta::find($id);

            if(empty($nome))
                $nome="";
            
            $porta->nome = $nome;
            $porta->save();
        
            return response()->json(["response" => "ok"]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([]);
        }
    }
    else{
        return response()->json([]);
    }
});


Route::post('/user/name', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $nome = $request->input("nome");

            $user = App\User::where("accesstoken",$request->input("accesstoken"))->first();
            
            $user->name = $nome;
            $user->save();
        
            return response()->json(["response" => "ok"]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([]);
        }
    }
    else{
        return response()->json([]);
    }
});


Route::post('/user/password', function (Request $request) {
    if(checkAccessToken($request->input("accesstoken"))){
        try {
            $oldPassword = $request->input("oldPassword");
            $newPassword = $request->input("newPassword");

            $user = App\User::where("accesstoken",$request->input("accesstoken"))
                    ->where('password',$oldPassword)
                    ->first();
            
            $user->password = $newPassword;
            $user->save();
        
            return response()->json(["response" => "ok"]);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json([]);
        }
    }
    else{
        return response()->json([]);
    }
});



function checkAccessToken($accesstoken){
	return App\User::where("accesstoken",$accesstoken)->exists();
}
