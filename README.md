# eLock Android

eLock Android is **Android application** developed for final project of **Mobile devices development** class. the golad of this project is to create an android application that would connect to a **backend API** that would have the list of digital keys taht a user can use to access doors of buildings. This application also had to be able to comunicate with the doors using **NFC** or **Bluetooth**.

This project was developed for a class for the **University of Aveiro**

![cap1](./images/cap1.PNG)
![cap2](./images/cap2.PNG)



## Core Tecnologies

* Android
* SQLite
* SharedPrefrences
* RecyclerView
* JSON
* Room
* NFC service
* Biometric Login



## Main Functionality

* [x] List Keys
* [x] Ask for Key
* [x] Remove key 
* [x] Login User with credencials
* [x] Check current User with Biometric
* [x] Request API
* [x] Use Recycler Views
* [x] Use NFC or Bluetooth


## Getting Started

1. `git clone ...`
2. Android
   1. Install Android APK file on device
3. Test Sever
   1. `cd` into testServer folder
   2. Run `composer install` to install dependencies
   3. Run `php artisan key:generate` to generate unique application key
   4. Run `php artisan migrate` to setup the database
   5. Run `php artisan db:seed` to insert default values in database
   6.  Copy project folder to web server
4.  Enjoy! 