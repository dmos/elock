package com.elock.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.elock.R;
import com.elock.TopBarClass;
import com.elock.adapters.doorAdapter;
import com.elock.models.DoorModel.Door;
import com.elock.models.DoorModel.DoorDatabase;

import java.util.List;


public class DashboardActivity extends AppCompatActivity implements LifecycleOwner, doorAdapter.onDoorListner {

    private static final String TAG = "DashboardActivity";

    RecyclerView rvDoors;
    DoorDatabase db;

    ImageView addImage;
    ImageView refreshImage;

    List<Door> doorsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //create topbar
        new TopBarClass(this,findViewById(R.id.topbarView),"Dashboard",false,true);

        rvDoors = (RecyclerView) findViewById(R.id.rvDoors);

        initDoors();

        //get Add image and bind activaty change
        addImage = (ImageView) findViewById(R.id.addImage);

        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addDoorIntent = new Intent(getApplicationContext(),AddDoorActivity.class);
                startActivity(addDoorIntent);
            }
        });


        //get Add image and bind activaty change
        refreshImage = (ImageView) findViewById(R.id.refreshImage);

        refreshImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.updateDatabase();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = getSharedPreferences("door",MODE_PRIVATE);
        sp.edit().clear().apply();
    }


    public void initDoors(){
        db = DoorDatabase.getInstance(this);
        db.setContext(this);

        doorsList = db.getAllDoors();


        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvDoors.setLayoutManager(layoutManager);
        rvDoors.hasFixedSize();

        doorAdapter adapter = new doorAdapter(doorsList, this, this); //primeiro é o context, segundo é da interface implemetada
        rvDoors.setAdapter(adapter);

        db.getAllDoorsLive().observe(this, doors -> {
            System.out.println("OBSERVER - onChange - called");
            adapter.setDoorsList(doors);
            doorsList = doors;
        });

        db.updateDatabase();
    }


    /* metedos da interface dos botões da recyclerview */

    @Override
    public void onOpenClick(int position) {
        Door door = doorsList.get(position);
        System.out.println("DEBUG : onOpenClick : " + door);
        Intent open = new Intent(getApplicationContext(), OpenDoorActivity.class);
        open.putExtra("id", door.getId());
        startActivity(open);
    }

    @Override
    public void onEditClick(int position) {
        Door door = doorsList.get(position);

        System.out.println("DEBUG : onEditClick : " + door);
        Intent edit = new Intent(getApplicationContext(), EditDoorActivity.class);
        edit.putExtra("id", door.getId());
        startActivity(edit);
    }

    @Override
    public void onRemoveClick(int position) {
        Door door = doorsList.get(position);
        System.out.println("DEBUG : onRemoveClick : " + door);
        removeAlert(this,door);
    }


    public void removeAlert(Activity a, Door door){
        AlertDialog.Builder builder = new AlertDialog.Builder(a);
        builder.setMessage("tem a certeza que pertende eleminar?").setCancelable(false);

        // Add the buttons
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                db.sendRemovedDoorServer(door);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.cancel();
            }
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
