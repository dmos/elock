package com.elock.models.UserModel;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.elock.modules.apiCalls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class User {

    private static final String TAG = "UserModel";

    private SharedPreferences sharedPreferences;
    private Context context;

    public User(Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences("login",context.MODE_PRIVATE);;
    }

    public boolean tryLogin( String email, String password){
        String url = "/user";
        String params = "email="+email+"&password="+password;
        apiCalls login = (apiCalls) new apiCalls(context,"try to login",url,"POST").execute(params);

        try {
            String ServerResponse = login.get();
            if(ServerResponse ==null || ServerResponse.length() == 0){
                Toast.makeText(context,"sem resposta do servidor", Toast.LENGTH_SHORT).show();
            }
            else{
                JSONArray array = new JSONArray(ServerResponse);
                if(array.length() == 0){
                    Toast.makeText(context,"Credenciais erradas", Toast.LENGTH_SHORT).show();

                }
                else{
                    JSONObject json = array.getJSONObject(0);
                    int id = json.getInt("id");
                    String name = json.getString("name");
                    String accesstoken = json.getString("accesstoken").length() != 0? json.getString("accesstoken") :  "";

                    //context.getSharedPreferences("login",Context.MODE_PRIVATE); //todo : deve estar a mais
                    sharedPreferences.edit().putBoolean("login",true).apply();
                    sharedPreferences.edit().putInt("id",id).apply();
                    sharedPreferences.edit().putString("name",name).apply();
                    sharedPreferences.edit().putString("accesstoken",accesstoken).apply();

                    return true;
                }
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void logout(){
        sharedPreferences.edit().clear().apply();
        Log.e(TAG,"shared pref: " + sharedPreferences.getBoolean("login",false));
        Toast.makeText(context, "Logout success", Toast.LENGTH_LONG).show();
    }

    public boolean isLoggin(){
        return sharedPreferences.getBoolean("login",false);
    }

    public String getAccessToken(){
        return sharedPreferences.getString("accesstoken","");
    }
    public String getUsername(){
        return sharedPreferences.getString("name","");
    }

    public boolean editUserName(String nome){
        String url = "/user/name";
        String params = "nome="+nome;
        apiCalls edit = (apiCalls) new apiCalls(context,"a submeter...",url,"POST").execute(params);

        try {
            String ServerResponse = edit.get();
            if(ServerResponse ==null || ServerResponse.length() == 0){
                Toast.makeText(context,"sem resposta do servidor", Toast.LENGTH_LONG).show();
            }
            else{
                JSONObject array = new JSONObject(ServerResponse);
                if(array.length() == 0){
                    Toast.makeText(context,"occorreu um erro", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(context,"Alteração com Sucesso", Toast.LENGTH_LONG).show();
                    sharedPreferences.edit().putString("name",nome).apply();
                    return true;
                }
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }


    public boolean editPassword(String oldPassword,String newPassword,String newPasswordConfirm){
        if(oldPassword.equals("") || newPassword.equals("") || newPasswordConfirm.equals("")){
            Toast.makeText(context,"Precisa de preencher todos os campos", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!newPassword.equals(newPasswordConfirm)){
            Toast.makeText(context,"Password nova não foi confirmada com sucesso", Toast.LENGTH_SHORT).show();
            return false;
        }

        String url = "/user/password";
        String params = "oldPassword="+oldPassword+"&newPassword="+newPassword;
        apiCalls edit = (apiCalls) new apiCalls(context,"a submeter...",url,"POST").execute(params);

        try {
            String ServerResponse = edit.get();
            if(ServerResponse ==null || ServerResponse.length() == 0){
                Toast.makeText(context,"sem resposta do servidor", Toast.LENGTH_SHORT).show();
            }
            else{
                JSONObject array = new JSONObject(ServerResponse);
                if(array.length() == 0){
                    Toast.makeText(context,"occorreu um erro", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(context,"Alteração com Sucesso", Toast.LENGTH_SHORT).show();
                    return true;
                }
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

}
