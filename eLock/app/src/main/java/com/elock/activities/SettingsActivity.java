package com.elock.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.elock.R;
import com.elock.TopBarClass;
import com.elock.models.UserModel.User;

public class SettingsActivity extends AppCompatActivity {
    private static final String TAG = "SettingsActivity";

    private User user;

    TextView lblUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        new TopBarClass(this,findViewById(R.id.topbarView),"Settings",true,false);

        user = new User(this);

        lblUserName = findViewById(R.id.lblUserName);
        lblUserName.setText(user.getUsername());
    }



    public void logout(View v){
        user.logout();
        this.finishAffinity();

    }

    public void editNome(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alterar Nome");

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if( user.editUserName(input.getText().toString()) )
                    lblUserName.setText(user.getUsername());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void editPassword(View v){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alterar Password");

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        // Set up the inputs
        final TextView lblOldPassword = new TextView(this);
        lblOldPassword.setText("Password Antiga");
        final EditText oldPassword = new EditText(this);
        oldPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

        final TextView lblNewPassword = new TextView(this);
        lblNewPassword.setText("Password Nova");
        final EditText newPassword = new EditText(this);
        oldPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

        final TextView lblNewPasswordConfirm = new TextView(this);
        lblNewPasswordConfirm.setText("Confirmação da Password Nova");
        final EditText newPasswordConfirm = new EditText(this);
        newPasswordConfirm.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);

        lblOldPassword.setPadding(20,0,0,0);
        lblNewPassword.setPadding(20,0,0,0);
        lblNewPasswordConfirm.setPadding(20,0,0,0);


        layout.addView(lblOldPassword);
        layout.addView(oldPassword);
        layout.addView(lblNewPassword);
        layout.addView(newPassword);
        layout.addView(lblNewPasswordConfirm);
        layout.addView(newPasswordConfirm);


        builder.setView(layout);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                user.editPassword(oldPassword.getText().toString(),
                        newPassword.getText().toString(),newPasswordConfirm.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

}
